import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk


class starRating(Gtk.Box):
    def __init__(self, storage, index):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.storage = storage
        self.movieId = index
        self.movie = self.storage.storage[index]
        self.rating = self.movie['rating']
        self.stars = []

        # Create star buttons

        button_1 = Gtk.Button.new()
        button_2 = Gtk.Button.new()
        button_3 = Gtk.Button.new()
        button_4 = Gtk.Button.new()
        button_5 = Gtk.Button.new()
        self.buttons = [
            button_1,
            button_2,
            button_3,
            button_4,
            button_5
        ]
        index = 1

        for idx, button in enumerate(self.buttons):
            button.set_relief(Gtk.ReliefStyle.NONE)
            if (idx + 1) <= self.rating:
                star_icon = Gtk.Image.new_from_icon_name('starred-symbolic', Gtk.IconSize.BUTTON)
            else:
                star_icon = Gtk.Image.new_from_icon_name('non-starred-symbolic', Gtk.IconSize.BUTTON)
            self.stars.append(star_icon)
            button.add(star_icon)
            button.connect('clicked', self.on_star_button_clicked, index)
            index += 1

            self.pack_start(button, True, True, 0)



    def on_star_button_clicked(self, widget, rating):

        for idx, star in enumerate(self.stars):
            if (idx + 1) <= rating:
                star.set_from_icon_name('starred-symbolic', Gtk.IconSize.BUTTON)
            else:
                star.set_from_icon_name('non-starred-symbolic', Gtk.IconSize.BUTTON)

        self.storage.storage[self.movieId]['rating'] = rating
        self.storage._save()