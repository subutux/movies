import json
import os
import uuid


class fileStorage(dict):
    def __init__(self, folder):
        self.folder = folder
        self.storage = {}
        self.filestorage = folder + 'movies.json'
        if os.path.isdir(folder):
            if os.path.exists(self.filestorage):
                with open(self.filestorage, 'r') as f:
                    self.storage = json.load(f)
            else:
                with open(self.filestorage, 'w') as f:
                    f.write('{}')
                    self.storage = {}

    def _save(self):
        with open(self.filestorage, 'w') as f:
            json.dump(self.storage, f)

    def append(self, filename, data, coverdata):
        self.storage[filename] = data
        coverfile = self.folder + 'covr-' + str(uuid.uuid4()) + '.jpg'
        with open(coverfile, 'wb') as fb:
            fb.write(coverdata)

        self.storage[filename]['cover'] = coverfile
        self._save()

    def __getattr__(self, filename):
        return self.storage[filename]




