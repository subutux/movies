# Movies

A simple movie manager, to manage your mp4/m4v movies in a slick,
nice looking way.

![](./screenshots/movies_main.png)
![](./screenshots/movies_detail.png)