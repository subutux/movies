#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
from gi.repository import GdkPixbuf 
from gi.repository.GdkPixbuf import Pixbuf, PixbufLoader 
from mutagen import mp4
from libs.storage import fileStorage
import subprocess, os, sys
from libs.widgets import starRating

storage = fileStorage('/tmp/movies/')

class signals():
    def __init__(self, builder):
        self.builder = builder
        self.movie_liststore = builder.get_object('movie_liststore')

    def onDeleteWindow(self, *args):
        Gtk.main_quit(*args)

    def on_movieview_selection_changed(self, selection):
        if not selection.get_selected_items():
            return True
        path = selection.get_selected_items()[0]
        x, xx, cell = selection.get_cursor()
        treeiter = self.movie_liststore.get_iter(path)
        print("Selected movie {movie}".format(
            movie=self.movie_liststore.get_value(treeiter, 1)))
        popover = self.builder.get_object('popover_movie')
        popover.connect('delete-event', lambda w, e: w.hide() or True)
        popover.set_modal(True)
        popover_poster = self.builder.get_object('popover_movie_poster')
        pstar = self.builder.get_object('placeholder_star_rating')
        movie = self.movie_liststore.get_value(treeiter, 2)
        movie = storage.storage[movie]
        if pstar.get_children():
            pstar.remove(pstar.get_children()[0])

        starring = starRating(storage, movie['file'])
        pstar.add(starring)

        self.builder.get_object('label_title').set_text(movie['title'])
        self.builder.get_object('label_year').set_text(movie['release'].split('-')[0])
        self.builder.get_object('label_desc').set_text(movie['description'])
        self.builder.get_object('button_play').set_tooltip_text(movie['file'])
        button_viewed = self.builder.get_object('button_viewed')
        button_viewed.set_tooltip_text(movie['file'])
        if movie['viewed'] == 0:
            button_viewed.set_active(False)
        else:
            button_viewed.set_active(True)

        poster_pixbuf = self.movie_liststore.get_value(treeiter, 0)
        popover_poster.set_from_pixbuf(poster_pixbuf)

        popover.show_all()

    def on_button_viewed_toggled(self, widget):
        movie = storage.storage[widget.get_tooltip_text()]
        if movie['viewed'] == 0:
            movie['viewed'] = 1
        else:
            movie['viewed'] = 0
        storage._save()

    def on_button_play_clicked(sef, widget):
        print ("Opening {0}".format(widget.get_tooltip_text()))
        uri = widget.get_tooltip_text()
        # Gtk.show_uri(Gdk.Screen.get_default(), uri, 0)

        if sys.platform.startswith('darwin'):
            subprocess.call(('open', uri))
        elif os.name == 'nt':
            os.startfile(uri)
        elif os.name == 'posix':
            subprocess.call(('xdg-open', uri))

    def on_toggle_search_toggled(self, widget):

        search_bar = self.builder.get_object('search_bar')
        if search_bar.get_search_mode():
            search_bar.set_search_mode(False)
        else:
            search_bar.set_search_mode(True)

    def on_import_button_clicked(self, widget):
        parent = self.builder.get_object('main_window')
        fileChooser = Gtk.FileChooserDialog("Please choose a file", parent,
                                            Gtk.FileChooserAction.OPEN,
                                            (Gtk.STOCK_CANCEL,
                                             Gtk.ResponseType.CANCEL,
                                             Gtk.STOCK_OPEN,
                                             Gtk.ResponseType.OK
                                             ))
        response = fileChooser.run()

        if response == Gtk.ResponseType.OK:
            f = mp4.MP4(fileChooser.get_filename())
            print(fileChooser.get_filename())

            if '©nam' in f.tags:
                title = f.tags['©nam'][0]
            else:
                title = os.path.basename(fileChooser.get_filename())
                title, xx = os.path.splitext(title)

            if '©ART' in f.tags:
                artists = f.tags['©ART']
            else:
                artists = []

            if '©gen' in f.tags:
                genre = f.tags['©gen'][0]
            else:
                genre = ''

            if '©day' in f.tags:
                release = f.tags['©day'][0]
            else:
                release = ''
            if 'ldes' in f.tags:
                description = f.tags['ldes'][0]
            else:
                description = ''

            if 'covr' in f.tags:
                covr = f.tags['covr'][0]
            else:
                covr = b'\0'

            storage.append(fileChooser.get_filename(),
                           {'title': title,
                            'artists': artists,
                            'genre': genre,
                            'release': release,
                            'description': description,
                            'rating': 0,
                            'viewed': 0,
                            'file': fileChooser.get_filename()
                            }, covr)

            coverbuff = PixbufLoader()
            coverbuff.write(covr)
            coverbuff.close()
            pixbuf = coverbuff.get_pixbuf()
            poster = pixbuf.scale_simple(200, 300,
                                         GdkPixbuf.InterpType.BILINEAR)

            liststore = self.builder.get_object('movie_liststore')
            liststore.append([
                poster,
                title,
                fileChooser.get_filename()
            ])

        else:
            print("Cancel")
        fileChooser.destroy()


class guiApp():

    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('movies.ui')
        self.builder.connect_signals(signals(self.builder))
        self.window = self.builder.get_object('main_window')
        self.window.connect('delete-event', Gtk.main_quit)
        self.movie_liststore = self.builder.get_object("movie_liststore")


    def run(self):

        for movie in storage.storage:
            movie = storage.storage[movie]
            poster = Pixbuf.new_from_file_at_scale(
                movie['cover'], 200, 300, True)
            self.movie_liststore.append([
                poster,
                movie['title'],
                movie['file']
            ])

        self.window.show_all()
        Gtk.main()


app = guiApp()
app.run()
